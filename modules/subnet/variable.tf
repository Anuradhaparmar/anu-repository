variable "pub_cidr_block" {
  description = "subnet cidr"
  type = string
  default = "10.0.1.0/24"
}

variable "pub_subnet_name" {
  description = "subnet names"
  type = string
  default = "jenkins-subnet"

}

variable "vpc_id" {
  description = "subnet vpc name"
  type = string
}

variable "availability_zone" {
  description = "availibility zone for subnet"
  type = string
  default = "ap-south-1a"
  
}
