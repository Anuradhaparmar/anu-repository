variable "ami" {
  description = "instance ami"
  type = string
  default = "ami-053b0d53c279acc90"
}

variable "instance_type" {
  description = "instance type"
  type = string
  default = "t2.medium"
}

variable "key_name" {
  description = "instance key name"
  type = string
  default = "001key"
}


variable "pub_instance_name" {
  description = "public instance name"
  type = string
  default = "jenkins-tool"
}

variable "subnet_id" {
  description = "subnet name"
  type = string

}

variable "security_groups" {
  description = "security group name"
  type = string

}


variable "availability_zone" {
  description = "availability zone for instance"
  type = string
  default = "us-east-1a"
}
