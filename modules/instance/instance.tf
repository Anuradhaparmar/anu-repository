resource "aws_instance" "pub-server" {
  ami = var.ami
  availability_zone = var.availability_zone
  instance_type = var.instance_type
  key_name = var.key_name
  subnet_id = var.subnet_id
  associate_public_ip_address = true
  vpc_security_group_ids = [var.security_groups]
  
  tags = {
    Name = var.pub_instance_name 
  }
}
