output "vpc" {
  value = module.vpc.vpc_id
}

output "subnet" {
  value = module.subnet.subnet
}

output "igw" {
  value = module.igw.igw_id
}

output "route_table1" {
  value = module.route-table.public_route_id
}

output "security_group" {
  value = module.security-group.sg-output
}

output "public_instance" {
  value = module.instance.instance1_id
}
